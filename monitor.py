from fsevents import Observer
from fsevents import Stream
from collections import deque
import types
import pprint
import os
import signal
import threading
import time
from sets import Set
import Queue
from config import config


IN_MODIFY = 0x00000002
IN_ATTRIB = 0x00000004
IN_CREATE = 0x00000100
IN_DELETE = 0x00000200
IN_MOVED_FROM = 0x00000040
IN_MOVED_TO = 0x00000080

NO_EVENT = 0




class Notifier(threading.Thread):
    
    
	def __init__(self,kEvent,kMonitor):
		threading.Thread.__init__(self)
		self.queueChangeEvent = kEvent
		self.dirMonitor = kMonitor
		print "intialized notifier"

	def run(self):
		while True:
			self.queueChangeEvent.wait()

			#get the next event
			print "New item added to the queue"
			
			nextEvent  = self.dirMonitor.getNextEvent()
			pprint.pprint(nextEvent)
			if nextEvent !=  None:
				if type(nextEvent) == types.ListType:
					if nextEvent[0].mask == IN_MOVED_FROM and nextEvent[1].mask == IN_MOVED_TO:
						#file moved from one dir to other dir
						if os.path.dirname(nextEvent[0].name) == os.path.dirname(nextEvent[1].name):
							print "File renamed"
						else:
							print "File moved"

				else:
					if nextEvent.mask == IN_MODIFY:
						print "--File Modified"
					elif nextEvent.mask == IN_DELETE:
						print "--file deleted"
					elif nextEvent.mask == IN_CREATE:
						print "--file created"
					elif nextEvent.mask == IN_ATTRIB:
						print "--attribute changed"






class Monitor:
	'Class to monitor specific paths'

	def __init__(self,kPath=None):
		"""
		init the monitor
		"""

		self.observer = Observer()
		self.path = kPath
		self.eventLog = {}
		self.sem = threading.Semaphore()
		self.eventq = deque() #Queue.Queue()	
		self.qChangeEvent = threading.Event()
		self.notifier = Notifier(self.qChangeEvent,self)

	def addPathToMonitor(self,kPath):
		"""
		add path to be monitored
		"""
		self.path = kPath


	def startMonitoring(self):
		print "Start monitoring..."
		self.observer.start()
		self.stream = Stream(self.__fileChanged,self.path,file_events=True)
		self.observer.schedule(self.stream)
		self.notifier.start()


	def stopMonitoring(self):
		self.observer.unschedule(self.stream)
		self.observer.stop()
		print "observer stopped"

	def getNextEvent(self):
		self.sem.acquire()
	
		pprint.pprint(self.eventq)
		pprint.pprint(self.eventLog)
		event = None
		if len(self.eventq) > 0:
			eventCookie = self.eventq.popleft()
			event = self.eventLog.get(eventCookie)
		
		pprint.pprint(self.eventq)
		pprint.pprint(self.eventLog)
        
		self.queueChangeEvent.clear()
        
		self.sem.release()
		return event


	def __addNewFileChangeEvent(self,kFileEvent):
		self.sem.acquire()
		if ( kFileEvent.mask == IN_MOVED_FROM or kFileEvent.mask == IN_MOVED_TO ) and kFileEvent.cookie != None:
			eventList = self.eventLog.get(kFileEvent.cookie)
			if eventList == None:
				eventList=[]
			eventList.append(kFileEvent)
			self.eventLog[kFileEvent.cookie] = eventList
		
		if kFileEvent.cookie == None:
			kFileEvent.cookie = time.time()
			self.eventLog[kFileEvent.cookie] = kFileEvent


		if kFileEvent.cookie not in self.eventq:
			self.eventq.append(kFileEvent.cookie)

	#	pprint.pprint(self.eventLog)
		print ""
	#	pprint.pprint(self.eventq)

		self.sem.release()



	def __fileChanged(self,kFileEvent):
		"""
		File changed
		"""
		#print "file changed"	


		pathcomponents = kFileEvent.name.split('/')
		pathcomponents.reverse()
		filename = pathcomponents[0]

		if filename[0] == '.':
			#print "Hidden file ignoring..."
			return


		if kFileEvent.mask == IN_MODIFY:
			if os.path.isfile(kFileEvent.name):
				pass#print "File modified"
			else:
				return

		elif kFileEvent.mask == IN_ATTRIB:
			print ""
			#print "Attribute changed"

		elif kFileEvent.mask == IN_CREATE:
			pass
			#print "New file created"

		elif kFileEvent.mask == IN_DELETE:
			pass
			#print "File delete"

		elif kFileEvent.mask == IN_MOVED_FROM and kFileEvent.cookie != None:
			self.__addNewFileChangeEvent(kFileEvent)

		elif kFileEvent.mask == IN_MOVED_TO and kFileEvent.cookie != None:
			self.__addNewFileChangeEvent(kFileEvent)
			self.qChangeEvent.set()

		if kFileEvent.cookie == None:
			self.__addNewFileChangeEvent(kFileEvent)
			self.qChangeEvent.set()




		#pprint.pprint(kFileEvent)
		#pprint.pprint(self.eventLog)

		#print "\n\n\n"

#######################################################################################
#singal handler for Ctrl-C
#path to monitor
#path_to_monitor = "/Users/abhineet/Github/py/drive"

path_to_monitor =  config["target_folder"] #os.getcwd()




kMonitor = Monitor(path_to_monitor);

def signalHandler(kSignum,kFrame):
	"""
	signal handler for Ctrl - C
	"""

	if kSignum == signal.SIGINT:
		print "signal caught"
		kMonitor.stopMonitoring()



if __name__ == '__main__':
	print "starting"
	signal.signal(signal.SIGINT,signalHandler)
	kMonitor.startMonitoring()
