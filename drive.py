#!/usr/bin/python

import httplib2
import pprint
import os
import re
import pickle

from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.file import Storage
from config import config



#move these to the config.py

# Copy your credentials from the APIs Console
CLIENT_ID = '1096638299250-s986e0tpb6mbhdlod7v5nf5dr2nmikb8.apps.googleusercontent.com'
CLIENT_SECRET = 'Ts6L1YnRE8MUoXcyGiVUp4Yf'

# Check https://developers.google.com/drive/scopes for all available scopes
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'

# Redirect URI for installed apps
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

# Path to the file to upload
FILENAME = 'document.txt'


FILEID = 0
LASTMODIFIEDDATE = 1



def isFile(kMimeType):
	return not (kMimeType.split('.').count('folder') == 1)




class Drive:
    'Class to manage multiple drive accounts. Each drive instance will monitor a target folder on file system and corresponding google drive account'
    def __init__(self,kTargetFolder):
        self.targetDir = kTargetFolder 
        self.storage = Storage('.testfile')
        self.credentials = self.storage.get()
        self.infoFile = '.filesinfo'
        self.pkl = None
        self.filesinfo = {}
        self.drive_service = None
        
    def setupTargetFolder(self):
        os.chdir(self.targetDir)
        try:
        	self.pkl = open(self.infoFile,"rb+")
        	self.filesinfo = pickle.load(self.pkl)
        	self.pkl.close()
        except IOError as e:
        	self.filesinfo = {}
        
        
    def authenticate(self):
        self.storage = Storage('.testfile')
        self.credentials = self.storage.get()
        
        if self.credentials == None:
        	flow = OAuth2WebServerFlow(CLIENT_ID, CLIENT_SECRET, OAUTH_SCOPE, REDIRECT_URI)
        	authorize_url = flow.step1_get_authorize_url()
        	print 'Go to the following link in your browser: ' + authorize_url
        	code = raw_input('Enter verification code: ').strip()
        	self.credentials = flow.step2_exchange(code)
        	#store the credentials and check for credentials before generating new credentials
        	self.storage.put(self.credentials)
        else:
        	print self.credentials
        
        # Create an httplib2.Http object and authorize it with our credentials
        http = httplib2.Http()
        http = self.credentials.authorize(http)
        self.drive_service = build('drive', 'v2', http=http)
        
        
        
    def serializeFileInfo(self):
        os.chdir(self.targetDir)
        print "inside exit handler",self.infoFile 
        self.pkl = open(self.infoFile,"wb+")
        print self.pkl
        pickle.dump(self.filesinfo,self.pkl)
        self.pkl.close()
        print os.getcwd()
        print "closed the file"
    
    def addEntryForFile(self,kFileId,kFileLocalPath,kLastModifiedDate):
    	"""
    	Fill in appropriate entries into file's info
    	"""
    	value  = [None,None]
    	value[FILEID] = kFileId
    	value[LASTMODIFIEDDATE] = kLastModifiedDate
    	self.filesinfo[kFileLocalPath] = value
    	#pprint.pprint(filesinfo)
    	#print "\n"
        
        
    def cloneDrive(self,kFolderid = 'root',level = 0):

    	"""
    		clone files on the drive to local target folder
            file is downloaded if file does not exist
            TODO:  compare last modified date for local and remote file if the file already exists

    	"""

    	params = {}
    	result = []
    	children = []
    	page_token = None
    	currDir = os.getcwd()
    	while True:
    		params['pageToken'] = page_token
    		children = self.drive_service.children().list(folderId = kFolderid,**params).execute()

    		for child in children.get('items'):

    			#get file's meta data
    			file = self.drive_service.files().get(fileId=child['id']).execute()

    			#if file is thrashed ignore
    			if file['labels']['trashed'] == True:
    				continue

    			isThisFile = isFile(file['mimeType'])

    			seperator = " "*level

    			if isThisFile ==  True:
    				#if file then clone
    				localFilePath = currDir+'/'+file['title']
    				fileTitle = file['title']
				
    				downloadurl = file.get('downloadUrl')
    				if downloadurl == None:
    					exportlinks = file.get('exportLinks')
    					if exportlinks:
    						downloadurl = exportlinks.get('application/pdf')
    						fileTitle = fileTitle+'.pdf'
    						localFilePath = localFilePath+'.pdf'
				
    				print seperator,"-",fileTitle," [in]  ",currDir," : ",os.path.exists(localFilePath),'  [SIZE] ',file.get('fileSize'),' [Last Modified] ',file.get('modifiedDate')

    				self.addEntryForFile(file['id'],localFilePath,file.get('modifiedDate'))
    				if not os.path.exists(localFilePath):

    					if  downloadurl != None:
    						responseCode, fileContent = self.drive_service._http.request(downloadurl)
    						if responseCode.status == 200:
    							print "Downloaded..."
    							f =open(localFilePath,'w+')
    							f.write(fileContent)
    							f.close()
    						else:
    							print "error downloading file"

    				else:
    					print "File already exists ",file['title']
    			else:
    				folderTitle = file['title']
    				folderLocalPath = currDir+'/'+ folderTitle

    				#if the folder does not exits create it	

    				if not os.path.exists(folderLocalPath):
    					print os.makedirs(folderLocalPath)

    				print folderLocalPath
    				os.chdir(folderLocalPath)
    				print seperator,"d",folderTitle
    				self.cloneDrive(file['id'],level+1)

    		page_token = children.get('nextPageToken')

    		if not page_token:
    			break

    
    
        
                





















if __name__ == '__main__':
    targetDir = config["target_folder"]
    myDrive = Drive(targetDir)
    myDrive.setupTargetFolder()
    myDrive.authenticate()
    myDrive.cloneDrive()
    myDrive.serializeFileInfo()
    




