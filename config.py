import os

target_folder_path = os.environ['HOME']+'/'+'drive'

if os.path.exists(target_folder_path) == False:
	os.mkdir(target_folder_path)
else:
	if os.path.isfile(target_folder_path):
		os.mkdir(target_folder_path)

config = {

	"target_folder": target_folder_path 

}
